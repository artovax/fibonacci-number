package com.example.artovax.fibonaccinumber.Unsigned;

import java.io.ObjectStreamException;
import java.math.BigInteger;
/**
 * Created by Artovax on 2015-06-27.
 */
public class UInteger extends Number  implements  Comparable<UInteger> {

    private static final Class<UInteger> CLASS = UInteger.class;
    private static final String  CLASS_NAME    = CLASS.getName();

    public BigInteger toBigInteger() {
        return new BigInteger(toString());
    }

    private static final String  PRECACHE_PROPERTY = CLASS_NAME + ".precacheSize";

    private static final int DEFAULT_PRECACHE_SIZE = 256;

    private static final long serialVersionUID  = -6821055240959745390L;

    private static final UInteger[]  VALUES= mkValues();

    public static final long MIN_VALUE = 0x00000000;

    public static final long MAX_VALUE = 0xffffffffL;

    private final long   value;

    private static final int getPrecacheSize() {

        String prop = null;
        long propParsed;

        try {
            prop = System.getProperty(PRECACHE_PROPERTY);
        }
        catch (SecurityException e) {
             return DEFAULT_PRECACHE_SIZE;
        }

        if (prop == null) {
            return DEFAULT_PRECACHE_SIZE;
        }

        if (prop.length() <= 0) {
            return DEFAULT_PRECACHE_SIZE;
        }

        try {
            propParsed = Long.parseLong(prop);
        }

        catch (NumberFormatException e) {
            return DEFAULT_PRECACHE_SIZE;
        }

        if (propParsed < 0) {
            return 0;
        }

        if (propParsed > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }

    return (int) propParsed;
    }

    private static final UInteger[] mkValues() {
        int precacheSize = getPrecacheSize();
        UInteger[] ret;

        if (precacheSize <= 0) {
            return null;
        }
        ret = new UInteger[precacheSize];
        for (int i = 0; i < precacheSize; i++) {
            ret[i] = new UInteger(i);
        }
        return ret;
    }

    private UInteger(long value, boolean unused) {
        this.value = value;
    }

    private static UInteger getCached(long value) {
        if (VALUES != null && value < VALUES.length) {
            return VALUES[(int) value];
        }
        return null;
    }


    private static UInteger valueOfUnchecked(long value) {
        UInteger cached;

        if ((cached = getCached(value)) != null) {
            return cached;
        }
        return new UInteger(value, true);
        }

    public static UInteger valueOf(String value) throws NumberFormatException {
        return valueOfUnchecked(rangeCheck(Long.parseLong(value)));
    }

    public static UInteger valueOf(int value) {
        return valueOfUnchecked(value & MAX_VALUE);
    }

    public static UInteger valueOf(long value) throws NumberFormatException {
        return valueOfUnchecked(rangeCheck(value));
    }

    private UInteger(long value) throws NumberFormatException {
        this.value = rangeCheck(value);
    }

    private UInteger(int value) {
         this.value = value & MAX_VALUE;
    }

    private UInteger(String value) throws NumberFormatException {
        this.value = rangeCheck(Long.parseLong(value));
    }

    private static long rangeCheck(long value) throws NumberFormatException {
        if (value < MIN_VALUE || value > MAX_VALUE) {
            throw new NumberFormatException("Value is out of range : " + value);
        }
        return value;
    }

    private Object readResolve() throws ObjectStreamException {
        UInteger cached;
        rangeCheck(value);
        if ((cached = getCached(value)) != null) {
            return cached;
        }
        return this;
    }

    @Override
    public int intValue() {
        return (int) value;
    }

    @Override
    public long longValue() {
        return value;
    }

    @Override
    public float floatValue() {
        return value;
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(value).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UInteger) {
            return value == ((UInteger) obj).value;
        }
        return false;
    }

    @Override
    public String toString() {
        return Long.valueOf(value).toString();
    }

    @Override
    public int compareTo(UInteger o) {
        return (value < o.value ? -1 : (value == o.value ? 0 : 1));
    }
}
