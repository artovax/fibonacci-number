package com.example.artovax.fibonaccinumber.Unsigned;

/**
 * Created by Artovax on 2015-06-27.
 */
import java.math.BigInteger;

public class Unsigned {

    public static UInteger uint(String value) throws NumberFormatException {
        return value == null ? null : UInteger.valueOf(value);
    }

    public static UInteger uint(int value) {
        return UInteger.valueOf(value);
    }


    public static UInteger uint(long value) throws NumberFormatException {
        return UInteger.valueOf(value);
    }

    private Unsigned() {}
}
