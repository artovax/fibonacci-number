package com.example.artovax.fibonaccinumber;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Artovax on 2015-06-27.
 */
public class FibonacciNumberAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;

    private List<String> mNumbers = new ArrayList<String>(); //
    public FibonacciNumberAdapter(Context mContext, ArrayList<String> mData) {
        super(mContext, R.layout.fibonacci_number_details, mData);

        this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mNumbers = new ArrayList<String>(mData.size());
    }

    public List<String> getmNumbers(){
        return Collections.unmodifiableList(mNumbers);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FibonacciNumberHolder mHolder;

        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.fibonacci_number_details, parent, false);
            mHolder = new FibonacciNumberHolder();
            TextView mFibonacciNumber = (TextView) convertView.findViewById(R.id.fibonacci_number);
            mHolder.mFibonacciNumber = mFibonacciNumber;
            convertView.setTag(mHolder); // związanie widoku z ViewHolderem
        }

        else
        {
            mHolder = (FibonacciNumberHolder) convertView.getTag();
        }

        final String mNumber = getItem(position);
        mHolder.mFibonacciNumber.setText(mNumber);
        return convertView;
    }

    public static class FibonacciNumberHolder {
        public TextView mFibonacciNumber;
    }
}
