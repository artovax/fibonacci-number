package com.example.artovax.fibonaccinumber;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TableLayout;


import com.example.artovax.fibonaccinumber.Unsigned.UInteger;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

import static com.example.artovax.fibonaccinumber.Unsigned.UInteger.valueOf;
import static com.example.artovax.fibonaccinumber.Unsigned.Unsigned.uint;


public class FibonacciActivity extends ActionBarActivity {

    UInteger  fib0 = uint(1);
    UInteger  fib1 = uint(1);
    UInteger  fib = uint(1);

    private ArrayList<String> mList = new ArrayList<>();

    protected ListView mListView;

    private FibonacciNumberAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fibonacci);
        mListView = (ListView) findViewById(R.id.fibonacci_container); // Znajdz widok o ID lista miast

        LoadList();
        mAdapter=new FibonacciNumberAdapter(this, mList);

        mListView.setAdapter(mAdapter);
    }

    protected void LoadList() {
        int i =2;
        long MaxUint = Long.parseLong(String.valueOf(Long.valueOf(String.valueOf(UInteger.MAX_VALUE))));
        fib0 = valueOf(0);
        fib1 = valueOf(1);
        mList = new ArrayList<String>();
        mList.add("F(0)="+fib0);
        mList.add("F(1)=" + fib1);

        while(Long.valueOf(String.valueOf(fib))<MaxUint){
            if(Long.valueOf(String.valueOf(fib0)) +  Long.valueOf(String.valueOf(fib1))>MaxUint){
                break;
            }
           fib = UInteger.valueOf(Long.valueOf(String.valueOf(fib0)) +  Long.valueOf(String.valueOf(fib1)));

           mList.add("F("+ i +")=" + fib);
           fib0 = fib1;
           fib1 = fib;
           i++;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fibonacci, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
